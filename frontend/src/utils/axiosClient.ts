import axios from "axios";
import { AdminUser, useAuthStore } from "../store/AuthStore";

let currentUser: AdminUser | null = null;
try {
    const authStore = useAuthStore();
    currentUser = authStore.currentUser;
} catch (error) {
    console.error("Error fetching user from authStore:", error);
    const userString = localStorage.getItem("user");
    if (userString) {
        currentUser = JSON.parse(userString);
    }
}

const axiosClient = axios.create({
    baseURL: import.meta.env.VITE_API_URL + "/api/v1",
    headers: {
        "Content-Type": "application/json",
        Authorization: "Basic " + btoa(currentUser?.Name ?? ""),
    },
});

export default axiosClient;
